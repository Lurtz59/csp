package Model.Tree;

import Model.CSP.Vertex;

import java.util.Comparator;

/**
 * Comparator allows you to sort the vertices by their name (ex: X1, X10, X2, X5 to X1, X2, X5, X10)
 */
public class NumericalStringComparator implements Comparator<Vertex> {
    public int compare(Vertex vertex1, Vertex vertex2) {
        Integer valX = Integer.parseInt(vertex1.getName().substring(1));
        Integer valY = Integer.parseInt(vertex2.getName().substring(1));
        return valX - valY;
    }
}
