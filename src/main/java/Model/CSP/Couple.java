package Model.CSP;

/**
 * Class for create a constraint between two vertices
 */

public class Couple {

    private int x;
    private int y;

    public Couple(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Couple couple = (Couple) o;
        return x == couple.x &&
                y == couple.y;
    }

    @Override
    public String toString() {
        return  "(" + x + "," + y + ")";
    }
}
