package Solvers;

import Model.CSP.CSP;
import Model.Tree.Node;

public class ForwardChecking {

    public void execute(CSP csp, StringBuilder stringBuilder) {
        int treeDepth = 0;
        Node root = new Node(SolversUtils.NAME_NODE_ROOT);
        root.extendForwardChecking(csp.getVertices().get(treeDepth), csp.getConstraints(), csp.getVertices());
        Node solution = forwardChecking(root, csp, treeDepth, stringBuilder);
        csp.setSolutionForwardChecking(solution);
    }

    private Node forwardChecking(Node current, CSP csp, int treeDepth, StringBuilder stringBuilder) {
        Node solution;

        // if we can't extend the branch anymore, there is a solution
        if (current.getChildren().isEmpty() || current.getChildren() == null) {
            solution = current;
            return solution;
        }

        for (Node child : current.getChildren()) {
            int treeDepthInProgress = treeDepth + 1;
            stringBuilder.append(SolversUtils.writeTreeExploration(child, treeDepthInProgress));

            // we test if the current node respects the constraints, if yes we continue to explore otherwise we change the son (we change the domain for our variable)
            if (treeDepth !=0 && !SolversUtils.verifyValidityNode(child, csp, treeDepth)) {
                continue;
            }

            // if is possible to expand, we expand the node in create thoses childrens (a child represents the variable X1 for domain 1, another son represents variable X1 for domain 2, etc.)
            boolean isPossibleToExpand = (treeDepthInProgress != csp.getVertices().size());
            if (isPossibleToExpand) {
                child.extendForwardChecking(csp.getVertices().get(treeDepthInProgress), csp.getConstraints(), csp.getVertices());
            }

            //we continue to explore the tree
            solution = forwardChecking(child, csp, treeDepthInProgress, stringBuilder);

            // if there is a solution, we return this solution to the parant
            boolean hasAnSolution = (solution != null);
            if (hasAnSolution) {
                return solution;
            }
        }
        return null;
    }
}
