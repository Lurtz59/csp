package Solvers;

import Model.CSP.CSP;
import Model.CSP.Couple;
import Model.Tree.NumericalStringComparator;
import Model.CSP.Vertex;
import Model.Tree.Node;

import java.util.*;

/**
 * A utility for algorithms
 */
public class SolversUtils {

    private static final String SEND_DEPTH = " Profondeur : ";
    private static final String SEND_VISITED_NODE = ", Noeud visité : ";
    private static final String SEND_DOMAIN = ", Domaine : ";
    private static final String SEND_PARENT_NODE = ", Noeud parent : ";
    private static final String STR = "------";
    protected static final String NAME_NODE_ROOT = "ROOT";

    /**
     * Allow to have the possible couples between each variable
     * @param vertices the list of vertices
     * @return the possible couples between each variable while respecting the links between each variable
     */
    public static Map<String, List<Couple>> getValueCouples(final List<Vertex> vertices) {
        Map<String, List<Couple>> possibleValueCouples = new HashMap<>();
        for(Vertex vertexX : vertices) {
            for(Vertex vertexY : vertices) {
                // if the vertex is not linked to another, we create all couples beetween himself and the vertex X, because all couples is accepted
                if(vertexY.getLinkedVertices().size() == 0) {
                    possibleValueCouples.put(vertexX.getName(), getValueCouples(vertexX, vertexY));
                // else
                } else {
                    boolean coupleExist =  vertexX.getLinkedVertices().get(vertexX.getName()).contains(vertexY.getName());
                    Integer valX = Integer.parseInt(vertexX.getName().substring(1));
                    Integer valY = Integer.parseInt(vertexY.getName().substring(1));
                    // allows to make desired couples, X1 with X2, X2 with 20 and not X20 with 2
                    boolean nameVertexXIsSmallerThanTheNameOfVertexY = valX.compareTo(valY) < 0;
                    if(coupleExist && nameVertexXIsSmallerThanTheNameOfVertexY) {
                        possibleValueCouples.computeIfAbsent(vertexX.getName(), k -> new ArrayList<>());
                        possibleValueCouples.get(vertexX.getName()).addAll(getValueCouples(vertexX, vertexY));
                    }
                }
            }
        }
        return possibleValueCouples;
    }

    /**
     * Allow to have the possible couples between vertexX and vertexY
     * @param vertexX vertexX
     * @param vertexY vertexY
     * @return possible value couples
     */
    public static List<Couple> getValueCouples(Vertex vertexX, Vertex vertexY) {
        List<Couple> couples = new ArrayList<>();
        for(Integer domainVertexX : vertexX.getDomains()) {
            for(Integer domainVertexY : vertexY.getDomains()) {
                couples.add(new Couple(domainVertexX, domainVertexY));
            }
        }
        return couples;
    }

    /**
     * Allows to obtain all the possible pairs of values ​​between the vertices
     * @param vertices vertices
     * @return map allowing to know all couples possible
     */
    public static Map<Vertex, List<Vertex>> getCouplesVertex(List<Vertex> vertices) {
        Map<Vertex, List<Vertex>> couples = new HashMap<>();
        for (Vertex vertexX : vertices) {
            couples.put(vertexX, new ArrayList<>());
            for (Vertex vertexY : vertices) {
                boolean isHimself = vertexX.equals(vertexY);
                boolean coupleExist = couples.get(vertexY) != null;
                if (isHimself || coupleExist) {
                    continue;
                }
                // we create a couple (vertex1, vertex2)
                couples.get(vertexX).add(vertexY);
            }
        }
        // Remove map empty
        couples.entrySet().removeIf(entry -> entry.getValue().isEmpty());
        return couples;
    }

    /**
     * Allow to round the decimal number to the higher number or lower number
     * @param number the number to round
     * @return the number rounded
     */
    public static int roundNumber(double number){
        double decimal = number - (int) number;
        if(decimal < 0.5) {
            number = Math.floor(number);
        } else {
            number = Math.ceil(number);
        }
        return (int) number;
    }

    /**
     * Show tree exploration
     * @param node node
     * @param depth the depth of tree
     */
    public static String writeTreeExploration(Node node, int depth) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(lineBreak());
        for (int i = 0; i < depth; i++) {
            stringBuilder.append(STR);
        }
        stringBuilder.append(SEND_DEPTH).append(depth).append(SEND_VISITED_NODE).append(node.getData().getName()).append(SEND_DOMAIN).append(node.getData().getDomains());
        if(node.getParent() != null) {
            stringBuilder.append(SEND_PARENT_NODE).append(node.getParent().getData().getName());
        }
        return stringBuilder.toString();
    }

    /**
     * Verify contraint for the node
     * @param node node
     * @param csp csp
     * @param depth depth
     * @return true if the node respect constraints else false
     */
    public static boolean verifyValidityNode(Node node, CSP csp, int depth) {
        boolean result = false;
        List<Vertex> verticesNode = getVertices(node, new ArrayList<>(), depth);
        Map<String, List<Couple>> couples = SolversUtils.getValueCouples(verticesNode);
        for(Map.Entry<String, List<Couple>> verticeCouple : couples.entrySet()) {
            result = csp.getConstraints().containsAll(verticeCouple.getValue());
            if(!result) {
                return result;
            }
        }
        return result;
    }


    /**
     * We recover the whole branch
     * @param node node
     * @param vertices the list of vertices, preferably empty
     * @param depth the depth
     * @return the list of vertices linked at the node
     */
    public static List<Vertex> getVertices(Node node, List<Vertex> vertices, int depth) {
        vertices.add(node.getData());
        if(depth == 0) {
            return vertices;
        } else {
            getVertices(node.getParent(), vertices, depth - 1);
        }
        vertices.sort(new NumericalStringComparator());
        return vertices;
    }

    public static String lineBreak() {
        return "\n";
    }

    /**
     * Find vertex by name
     * @param vertices the list of vertices
     * @param name name of vertex
     * @return vertex find
     */
    public static Vertex findVertexByName(List<Vertex> vertices, String name) {
        return vertices.stream().filter(vertex -> vertex.getName().equals(name)).findFirst().orElse(null);
    }
}
