package Solvers;

import Model.CSP.CSP;
import Model.CSP.Couple;
import Model.CSP.Vertex;
import Model.Tree.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Backjumping {

    public void execute(CSP csp, StringBuilder stringBuilder) {
        int treeDepth = 0;
        Node root = new Node(SolversUtils.NAME_NODE_ROOT);
        root.extend(csp.getVertices().get(treeDepth));
        Node solution = backjumping(root, csp, treeDepth, stringBuilder, new ArrayList<>());
        csp.setSolutionBackJumping(solution);
    }

    private Node backjumping(Node current, CSP csp, int treeDepth, StringBuilder stringBuilder, List<Node> conflicts) {
        Node solution;

        // if we can't extend the branch anymore, there is a solution
        if (current.getChildren().isEmpty() || current.getChildren() == null) {
            solution = current;
            return solution;
        }

        // if the node has already been visited, we return null
        if (current.isVisited()) {
            return null;
        }

        // if the node is not equal to root, we mark the node
        if(treeDepth != 0) {
            current.setVisited(true);
        }

        for (Node child : current.getChildren()) {
            // if the node children is visited, we continue with the next children
            if(child.isVisited()) {
                continue;
            }

            int treeDepthProgress = treeDepth + 1;
            stringBuilder.append(SolversUtils.writeTreeExploration(child, treeDepthProgress));

            // we test if the current node respects the constraints, if yes we continue to explore otherwise we change the son (we change the domain for our variable)
            if (treeDepth !=0 && !SolversUtils.verifyValidityNode(child, csp, treeDepth)) {
                // we mark our son
                child.setVisited(true);
                // we add it to the conflict list
                conflicts.add(child);
                // if we have visited the entire domain of the variable and its domain respects no constraints
                boolean domainViolatesAllConstraints = conflicts.size() == csp.domainSizeByVertexName(child.getData().getName());
                if (domainViolatesAllConstraints) {
                    // We recover the last value of the domain
                    Node lastNode = conflicts.get(conflicts.size()-1);
                    // We clear the list of conflicts
                    conflicts.clear();
                    // We recover the whole branch
                    List<Vertex> verticesNode = SolversUtils.getVertices(lastNode, new ArrayList<>(), treeDepth);
                    // We create the pairs of possible values
                    Map<String, List<Couple>> couples = SolversUtils.getValueCouples(verticesNode);
                    // We remove the couples not concerned
                    couples.forEach((key, value) -> value.removeIf(couple -> (couple.getY() != child.getData().getDomains().get(0))));
                    //With the last value of the domain, we look at the closest violated constraint, as soon as we find it, we perform a backjumping on this node
                    ArrayList<String> keyList = new ArrayList(couples.keySet());
                    for (int i = keyList.size() - 1; i >= 0; i--) {
                        // The last node explored
                        String key = keyList.get(i);
                        for (Couple couple :  couples.get(key)) {
                            // we check if the last node to violate a constraint
                            if(!csp.getConstraints().contains(couple)) {
                                String nameVertexBackjump = csp.whoHasThisDomain(couple.getX());
                                // we recover the node which violated this constraint by its name in our branch
                                Node node = findNodeByName(lastNode, nameVertexBackjump);
                                // we unmark the node and do a backjump on it
                                node.setVisited(false);
                                return backjumping(node, csp, Integer.parseInt(nameVertexBackjump.substring(1))-1, stringBuilder, conflicts);
                            }
                        }
                    }
                }
                continue;
            }
            // if the child respects the constraints, we delete the list of conflicts
            conflicts.clear();

            // if is possible to expand, we expand the node in create thoses childrens (a child represents the variable X1 for domain 1, another son represents variable X1 for domain 2, etc.)
            boolean isPossibleToExpand = (treeDepthProgress != csp.getVertices().size());
            if (isPossibleToExpand) {
                child.extend(csp.getVertices().get(treeDepthProgress));
            }

            //we continue to explore the tree
            solution = backjumping(child, csp, treeDepthProgress, stringBuilder, conflicts);

            // if there is a solution, we return this solution to the parant
            boolean hasAnSolution = (solution != null);
            if (hasAnSolution) {
                return solution;
            }

        }

        return null;
    }

    /**
     * research a node specific by name in the branch
     * @param node node
     * @param name name of node
     * @return node search
     */
    private Node findNodeByName(Node node, String name) {
        if(!node.getData().getName().equals(name)) {
            return findNodeByName(node.getParent(), name);
        }
        return node.getParent();
    }
}
